import BlacknetStore from './blacknetStore';

class Stores {
  constructor() {
    this.blacknetStore = new BlacknetStore()
  }
}

export default new Stores();
