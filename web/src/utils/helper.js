import axios from 'axios';
import config from '@/config';

const apiVersion = '/api/v2';
const { BigNumber } = window;

axios.interceptors.request.use((conf) => {
  conf.url = `${apiVersion}${conf.url}`; // eslint-disable-line
  return conf;
});

axios.interceptors.response.use((response) => response.data, (error) => Promise.reject(error));

export function verifyAccount(account) {
  if (Object.prototype.toString.call(account) === '[object String]'
      && account.length > 21 && (/^blacknet[a-z0-9]{59}$/.test(account) || /^rblacknet[a-z0-9]{59}$/.test(account))) {
    return true;
  }
  return false;
}

export function verifyMnemonic(mnemonic) {
  if (Object.prototype.toString.call(mnemonic) === '[object String]'
      && mnemonic.split(' ').length === 12) {
    return true;
  }
  return false;
}

export function postPromise(url, data, isNeedAlert) {
  const formData = new FormData();
  Object.keys(data).forEach((k) => {
    formData.set(k, data[k]);
  });
  return axios({
    method: 'post',
    url,
    data: formData,
    headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' },
  }).catch((res) => {
    if (isNeedAlert && res.data.responseText) alert(res.data.responseText);
  });
}

export function toBLNString(number) {
  return `${new BigNumber(number).dividedBy(1e8).toFixed(8)} BLN`;
}

export function throttle(fn, threshhold = 250) {
  let last;
  let timer;

  return function (...args) {
    const context = this;
    const now = +new Date();
    if (last && now < last + threshhold) {
      clearTimeout(timer);

      timer = setTimeout(() => {
        last = now;
        fn.apply(context, args);
      }, threshhold);
    } else {
      last = now;
      fn.apply(context, args);
    }
  };
}

export function unix_to_local_time(unix_timestamp) {
  const date = new Date(unix_timestamp * 1000);
  const hours = `0${date.getHours()}`;
  const minutes = `0${date.getMinutes()}`;
  const seconds = `0${date.getSeconds()}`;
  const day = date.getDate();
  const year = date.getFullYear();
  const month = date.getMonth() + 1;

  return `${year}-${(`0${month}`).substr(-2)}-${
    (`0${day}`).substr(-2)} ${hours.substr(-2)}:${minutes.substr(-2)}:${seconds.substr(-2)}`;
}

export function getTxTypeName(type, tx, account) {
  const typeNames = [
    'Transfer',
    'Burn',
    'Lease',
    'CancelLease',
    'Bundle',
    'CreateHTLC',
    'UnlockHTLC',
    'RefundHTLC',
    'SpendHTLC',
    'CreateMultisig',
    'SpendMultisig',
    'WithdrawFromLease',
    'ClaimHTLC',
  ];

  let name = typeNames[type];

  if (type == 16) {
    name = 'MultiData';
  } else if (type == 254) {
    name = 'Generated';
  } else if (type == 0) {
    if (tx.from == account) {
      name = 'Sent to';
    } else {
      name = 'Received from';
    }
  }
  return name;
}

export function getFormatBalance(balance) {
  return `${new BigNumber(balance).dividedBy(1e8).toFixed(8)} BLN`;
}

/* Passed Dom Operation below */

const { GENESIS_TIME } = config;
let startTime;
let timeBehindText;

export async function renderProgressBar(timestamp) {
  const progressStats = document.querySelector('.progress-stats, .progress-stats-text');
  const secs = Date.now() / 1000 - timestamp; let pecent;
  const HOUR_IN_SECONDS = 60 * 60;
  const DAY_IN_SECONDS = 24 * 60 * 60;
  const WEEK_IN_SECONDS = 7 * 24 * 60 * 60;
  const YEAR_IN_SECONDS = 31556952; // Average length of year in Gregorian calendar

  if (secs < 5 * 60) {
    timeBehindText = undefined;
  } else if (secs < 2 * DAY_IN_SECONDS) {
    timeBehindText = `${(secs / HOUR_IN_SECONDS).toFixed(2)} hour(s)`;
  } else if (secs < 2 * WEEK_IN_SECONDS) {
    timeBehindText = `${(secs / DAY_IN_SECONDS).toFixed(2)} day(s)`;
  } else if (secs < YEAR_IN_SECONDS) {
    timeBehindText = `${(secs / WEEK_IN_SECONDS).toFixed(2)} week(s)`;
  } else {
    const years = secs / YEAR_IN_SECONDS;
    const remainder = secs % YEAR_IN_SECONDS;
    timeBehindText = `${years.toFixed(2)} year(s) and ${remainder.toFixed(2)}week(s)`;
  }

  if (!startTime) {
    startTime = GENESIS_TIME;
  }

  if (timeBehindText === undefined) {
    progressStats.style.display = 'none';
    return;
  }

  const totalSecs = Date.now() / 1000 - startTime;
  pecent = (secs * 100) / totalSecs;

  pecent = 100 - pecent;

  document.querySelector('.progress-bar').style.width = `${pecent}`;
  document.querySelector('.progress-stats-text').innerText = `${timeBehindText} behind`;
}

export function showProgress() {
  const progressStats = document.querySelector('.progress-stats, .progress-stats-text');
  if (timeBehindText !== undefined) {
    progressStats.style.display = 'block';
  }
}
