import React from 'react';
import { FormattedMessage } from '@/utils/locale';

const Lease = () => {
	return (
		<div className="lease">
			<div className="form">
				<legend><FormattedMessage id="lease bln" /></legend>
				<div className="field-row">
					<label><FormattedMessage id="lease to" /></label>
					<input type="text" id="lease_to" placeholder="Blacknet account..." />
				</div>

				<div className="field-row">
					<label>
						<span><FormattedMessage id="amount" /></span>
						<strong>(BLN)</strong>
					</label>
					<input type="number" placeholder="Amount" step="0.00000001" min="1" id="lease_amount" />
				</div>
				<div className="field-row">
					<label>
						<span><FormattedMessage id="fee" /></span>
						<strong>(BLN)</strong>
					</label>
					<input type="text" id="lease_fee" defaultValue='0.001' />
				</div>
				<button id="lease"><FormattedMessage id="lease" /></button>
			</div>
			<div className="field-row field-row-outer hidden">
				<label><FormattedMessage id="result" /></label>
				<p id="lease_result"></p>
			</div>
		</div>
	)
}

export default Lease;