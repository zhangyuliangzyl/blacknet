import React from 'react';
import { Form, Icon, Input, Button } from 'antd';
import { FormattedMessage } from '@/utils/locale';
import config from '@/config';
import './index.less';

class Config extends React.Component {
  onSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        console.log('Received values of form: ', values);
        config.explorer = values;
        localStorage.setItem('explorer', JSON.stringify(obj));
      }
    });
  };

  render() {
    const { explorer: { block, blockHeight, tx, account } } = config;
    const { getFieldDecorator } = this.props.form;
    return (
      <div className="tabpage-config-container">
        <div className="form">
          <legend><FormattedMessage id="explorer" /></legend>
          <Form layout="inline" labelAlign="left" onSubmit={this.onSubmit}>
            <Form.Item
                label={<FormattedMessage id="Block" />}
              >
                {getFieldDecorator('block', {
                  initialValue: block
                })(
                  <Input style={{ width: '495px' }} />,
                )}
            </Form.Item>
            <Form.Item
                label={<FormattedMessage id="Block Height" />}
              >
                {getFieldDecorator('blockHeight', {
                  initialValue: blockHeight
                })(
                  <Input style={{ width: '495px' }} />,
                )}
            </Form.Item>
            <Form.Item
                label={<FormattedMessage id="Transaction" />}
              >
                {getFieldDecorator('tx', {
                  initialValue: tx
                })(
                  <Input style={{ width: '495px' }} />,
                )}
            </Form.Item>
            <Form.Item
                label={<FormattedMessage id="Account" />}
              >
                {getFieldDecorator('account', {
                  initialValue: account
                })(
                  <Input style={{ width: '495px' }} />,
                )}
            </Form.Item>
            <Form.Item
              wrapperCol={{
                xs: { span: 24, offset: 0 },
                sm: { span: 16, offset: 8 },
              }}
            >
              <Button type="primary" htmlType="submit">
                <FormattedMessage id="Save">Local Save</FormattedMessage>
              </Button>
          </Form.Item>
          </Form>
        </div>
      </div>
    )
  }
}

export default Form.create()(Config)