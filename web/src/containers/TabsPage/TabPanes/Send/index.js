import React from 'react';
import { FormattedMessage } from '@/utils/locale';

const Send = () => {
	return (
		<div className="send">
			<div className="form">
				<legend><FormattedMessage id="send bln" /></legend>
				<div className="field-row">
					<label><FormattedMessage id="pay to" /></label>
					<input type="text" id="transfer_to" placeholder="Blacknet account..." />
				</div>

				<div className="field-row">
					<label>
						<span><FormattedMessage id="amount" /></span>
						<strong>(BLN)</strong>
					</label>
					<input type="number" step="0.00000001" min="1" placeholder="Amount" id="transfer_amount" />
				</div>

				<div className="field-row">
					<label><FormattedMessage id="message" /></label>
					<input type="text" id="transfer_message" placeholder="Transfer message" />
				</div>
				<div className="field-row">
					<label><FormattedMessage id="encrypted" /></label>
					<input type="checkbox" id="transfer_encrypted" />
				</div>
				<div className="field-row">
					<label>
						<span><FormattedMessage id="fee" /></span>
						<strong>(BLN)</strong>
					</label>
					<input type="text" id="transfer_fee" defaultValue='0.001' />
				</div>
				<button id="transfer"><FormattedMessage id="send" /></button>
			</div>
			<div className="field-row field-row-outer hidden">
				<label><FormattedMessage id="result" /></label>
				<p id="transfer_result"></p>
			</div>
		</div>
	)
}

export default Send;
