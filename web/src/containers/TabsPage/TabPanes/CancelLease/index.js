import React from 'react';
import { Table } from 'antd';
import axios from 'axios';
import config from '@/config';
import { FormattedMessage } from '@/utils/locale';

const { Column } = Table;

export default class CancelLease extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      noData: false,
      loading: true,
    };
    this.account = localStorage.account;
  }

  componentDidMount() {
    this.getRelease();
  }

  getRelease = async () => {
    const outLeases = await axios.get(`/wallet/${this.account}/outleases`);
    if (!outLeases.length) {
      this.setState({ loading: false, noData: true });
    }
    // $('.cancel_lease_tab').show();

    // if (outLeases.length > 0) {

    //  $('#leases-list').html('');
    //  outLeases.map(Blacknet.template.lease);

    // }
  }

  onCancle = () => {}

  render() {
    const { loading, noData } = this.state;
    return (
      <div className="cancel_lease">
        <h3><FormattedMessage id="Out Leases" /></h3>
        <Table
          dataSource={[]}
          footer={() => noData ? (
            <div className="tx-foot">
              <div className="no_tx_yet">
                <div className="text-center">
                  <p><span><FormattedMessage id="No transactions yet" /></span></p>
                </div>
              </div>
            </div>
          ) : null}
          loading={loading}
        >
          <Column
            title={<FormattedMessage id="Index" />}
            align="center"
            key="index"
            render={(text, record, index) => index + 1}
          />
          <Column
            title={<FormattedMessage id="Account" />}
            align="center"
            key="index"
            dataIndex="publicKey"
            render={(text) => <a target="_blank" href={`${config.explorer.account}${text}`}>{text}</a>}
          />
          <Column
            title={<FormattedMessage id="Height" />}
            align="center"
            key="height"
            dataIndex="height"
          />
          <Column
            title={<FormattedMessage id="Amount" />}
            align="center"
            key="amount"
            render={(text) => getFormatBalance(text)}
          />
          <Column
            title={<FormattedMessage id="Cancel Lease" />}
            align="center"
            key="btn"
            render={(text, record) => {
              const { publicKey, amount, height } = record;
              return (
                <a href="#" className="cancel_lease_btn" onClick={this.onCancle}>
                  Cancel
                </a>
              );
            }}
          />
        </Table>
        <table id="leases-table">
          <thead>
            <tr>
              <td />
              <td><FormattedMessage id="Account" /></td>
              <td><FormattedMessage id="Height" /></td>
              <td><FormattedMessage id="Amount" /></td>
              <td><FormattedMessage id="Cancel Lease" /></td>
            </tr>
          </thead>
          <tbody id="leases-list">
            <tr>
              <td colSpan="5" className="text-center">No out leases yet</td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
