import React from 'react';
import { FormattedMessage } from '@/utils/locale';

const MnemonicInfo = () => {
  return (
    <div className="info">
      <div className="form">
        <legend><FormattedMessage id="mnemonic info" /></legend>
        <div className="field-row">
          <label><FormattedMessage id="mnemonic" /></label>
          <input type="password" id="mnemonic_info_mnemonic" />
        </div>

        <button id="mnemonic_info"><FormattedMessage id="mnemonic info" /></button>
      </div>
      <div className="field-row field-row-outer hidden">
        <label><FormattedMessage id="result" /></label>
        <pre id="mnemonic_info_result"></pre>
      </div>
    </div>
  )
};

export default MnemonicInfo;