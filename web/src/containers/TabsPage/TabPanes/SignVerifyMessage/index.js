import React from 'react';
import { FormattedMessage } from '@/utils/locale';

const SignVerifyMessage = () => {
  return (
    <div className="sign">
      <div className="form">
        <legend><FormattedMessage id="sign message" /></legend>
        <div className="field-row">
          <label><FormattedMessage id="mnemonic" /></label>
          <input type="password" id="sign_mnemonic" />
        </div>
        <div className="field-row">
          <label><FormattedMessage id="message" /></label>
          <input type="text" id="sign_message" />
        </div>
        <button id="sign"><FormattedMessage id="sign message" /></button>
      </div>
      <div className="field-row field-row-outer hidden">
        <label><FormattedMessage id="result" /></label>
        <p id="sign_result"></p>
      </div>
      <div className="form">
        <legend><FormattedMessage id="verify message" /></legend>
        <div className="field-row">
          <label><FormattedMessage id="account" /></label>
          <input type="text" id="verify_account" />
        </div>
        <div className="field-row">
          <label><FormattedMessage id="signature" /></label>
          <input type="text" id="verify_signature" />
        </div>
        <div className="field-row">
          <label><FormattedMessage id="message" /></label>
          <input type="text" id="verify_message" />
        </div>
        <button id="verify"><FormattedMessage id="verify message" /></button>
      </div>
      <div className="field-row field-row-outer hidden">
        <label><FormattedMessage id="result" /></label>
        <p id="verify_result"></p>
      </div>
    </div>
  );
}

export default SignVerifyMessage;