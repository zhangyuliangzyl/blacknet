import React from 'react';
import { FormattedMessage } from '@/utils/locale';

const Staking = () => (
  <div className="staking">
    <h3><FormattedMessage id="staking info" /></h3>
    <table>
      <tbody>
        <tr>
          <td><FormattedMessage id="is staking" /></td>
          <td className="is_staking">{localStorage.isStaking || 'Press Refresh'}</td>
        </tr>
        <tr>
          <td><FormattedMessage id="start/stop/refresh" /></td>
          <td>
            <button id="refresh_staking"><FormattedMessage id="refresh" /></button>
            <button id="stop_staking"><FormattedMessage id="stop staking" /></button>
            <button id="start_staking"><FormattedMessage id="start staking" /></button>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
);

export default Staking;
