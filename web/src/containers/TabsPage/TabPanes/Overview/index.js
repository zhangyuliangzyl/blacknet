import React from 'react';
import { inject, observer } from 'mobx-react';
import axios from 'axios';
import { FormattedMessage } from '@/utils/locale';
import { toBLNString, unix_to_local_time } from '@/utils/helper';
import config from '@/config';

const { BigNumber } = window;

@inject('blacknetStore')
@observer
class Overview extends React.Component {
  constructor(props) {
    super(props);
    this.account = localStorage.account;
    this.state = { balance: {}, staking: {} };
  }

  componentDidMount() {
    this.getBalance();
    this.getStaking();
  }

  getBalance = async () => {
    try {
      const { balance, confirmedBalance, stakingBalance } = await axios.get(`/account/${this.account}`);
      this.setState({
        balance: {
          balance: toBLNString(balance),
          confirmedBalance: toBLNString(confirmedBalance),
          stakingBalance: toBLNString(stakingBalance),
        },
      });
    } catch (e) {
      this.setState({ balance: {} });
    }
  };

  getStaking = async () => {
    const staking = await axios.get('/staking');
    this.setState({ staking });
  }

  render() {
    const { balance, staking } = this.state;
    const { nodeInfo, ledger } = this.props.blacknetStore;

    return (
      <div className="overview">
        <h3><FormattedMessage id="Wallet Info" /></h3>
        <table>
          <tbody>
            <tr>
              <td><FormattedMessage id="account" /></td>
              <td>
                <span className="overview_account">{this.account || ''}</span>
                <a href="#" id="switch_account">
                  {' '}
                  <FormattedMessage id="log out" />
                </a>
              </td>
            </tr>
            <tr>
              <td><FormattedMessage id="balance" /></td>
              <td className="overview_balance strong">{balance.balance || '0.00000000 BLN'}</td>
            </tr>
            <tr>
              <td><FormattedMessage id="confirmed balance" /></td>
              <td className="overview_confirmed_balance strong">{balance.confirmedBalance || '0.00000000 BLN'}</td>
            </tr>
            <tr>
              <td><FormattedMessage id="staking balance" /></td>
              <td className="overview_staking_balance strong">{balance.stakingBalance || '0.00000000 BLN'}</td>
            </tr>
          </tbody>
        </table>
        <h3><FormattedMessage id="node info" /></h3>
        <table>
          <tbody>
            {
              nodeInfo.warnings.length > 0 ? (
                <tr className="overview_warnings_row">
                  <td>Warnings</td>
                  <td>
                    <span className="overview_warnings">{nodeInfo.warnings.join(',')}</span>
                  </td>
                </tr>
              ) : null
            }
            <tr>
              <td><FormattedMessage id="version" /></td>
              <td width="636">
                <span className="overview_version">{nodeInfo.version}</span>
              </td>
            </tr>
            <tr>
              <td><FormattedMessage id="height" /></td>
              <td>
                <a target="_blank" href={`${config.explorer.blockHeight}${ledger.height}`} className="overview_height">{ledger.height}</a>
              </td>
            </tr>
            <tr>
              <td><FormattedMessage id="blockhash" /></td>
              <td>
                <a target="_blank" href={`${config.explorer.block}${ledger.blockHash}`} className="overview_blockHash">{ledger.blockHash}</a>
              </td>
            </tr>
            <tr>
              <td><FormattedMessage id="blocktime" /></td>
              <td>
                <span className="overview_blockTime">{unix_to_local_time(ledger.blockTime)}</span>
              </td>
            </tr>
            <tr>
              <td><FormattedMessage id="difficulty" /></td>
              <td>
                <span className="overview_difficulty">{ledger.difficulty}</span>
              </td>
            </tr>
            <tr>
              <td><FormattedMessage id="cumulative difficulty" /></td>
              <td>
                <span className="overview_cumulativeDifficulty">{ledger.cumulativeDifficulty}</span>
              </td>
            </tr>
            <tr>
              <td><FormattedMessage id="supply" /></td>
              <td>
                <span className="overview_supply strong">{`${new BigNumber(ledger.supply).dividedBy(1e8)} BLN`}</span>
              </td>
            </tr>
          </tbody>
        </table>
        <h3><FormattedMessage id="staking info" /></h3>
        <table className="staking_info">
          <tbody>
            {
              nodeInfo.warnings.length > 0 ? (
                <tr className="overview_warnings_row">
                  <td>Staking Accounts</td>
                  <td>
                    <span className="stakingAccounts" />
                  </td>
                </tr>
              ) : null
            }
            <tr>
              <td><FormattedMessage id="weight" /></td>
              <td width="636">
                <span className="weight">{staking.weight || ''}</span>
              </td>
            </tr>
            <tr>
              <td><FormattedMessage id="networkWeight" /></td>
              <td>
                <span className="networkWeight">{staking.networkWeight}</span>
              </td>
            </tr>
            <tr>
              <td><FormattedMessage id="expectedTime" /></td>
              <td>
                <span className="expectedTime">{staking.expectedTime}</span>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}
export default Overview;
