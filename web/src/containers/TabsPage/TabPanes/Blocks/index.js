import React from 'react';
import { inject, observer } from 'mobx-react';
import axios from 'axios';
import { Table } from 'antd';
import { FormattedMessage } from '@/utils/locale';
import config from '@/config';
import { unix_to_local_time } from '@/utils/helper';

const { Column } = Table;

@inject('blacknetStore')
@observer
class Blocks extends React.Component {
  constructor(props) {
    super(props);
    this.state = { blockList: [] };
  }

  componentDidMount() {
    const { blacknetStore: { onInitResolve } } = this.props;
    onInitResolve(this.initRecentBlocks);
  }

  addBlock = async (hash, height) => {
    const url = `/block/${hash}`;
    const block = await axios(url);
    block.height = height;
    const { blockList } = this.state;
    this.setState({ blockList: [...blockList, block] });

    return block.previous;
  }

  initRecentBlocks = async ({ ledger }) => {
    let { blockHash: hash, height } = ledger;
    let i = 0;
    if (height < 36) return;

    while (i++ < 35) {
      hash = await this.addBlock(hash, height);
      height--;
    }
  }

  render() {
    const { blockList } = this.state;
    return (
      <div className="recentblocks">
        <h3><FormattedMessage id="blocks" /></h3>
        <Table dataSource={blockList} rowKey="height" pagination={false}>
          <Column
            title={<FormattedMessage id="height" />}
            key="height"
            dataIndex="height"
            render={(text) => <a target="_blank" href={`${config.explorer.blockHeight}${text}`}>{text}</a>}
            align="center"
          />
          <Column
            title={<FormattedMessage id="size" />}
            key="size"
            dataIndex="size"
            align="center"
          />
          <Column
            title={<FormattedMessage id="time" />}
            key="time"
            dataIndex="time"
            render={(text) => unix_to_local_time(text)}
            align="center"
          />
          <Column
            title={<FormattedMessage id="txns" />}
            key="transactions"
            dataIndex="transactions"
            align="center"
          />
          <Column
            title={<FormattedMessage id="generator" />}
            key="generator"
            dataIndex="generator"
            render={(text) => <a target="_blank" href={`${config.explorer.account}${text}`}>{text}</a>}
            align="center"
          />
        </Table>
      </div>
    );
  }
}

export default Blocks;
