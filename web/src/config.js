export default {
  explorer: {
    block: 'https://blnscan.io/',
    blockHeight: 'https://blnscan.io/',
    tx: 'https://blnscan.io/',
    account: 'https://blnscan.io/',
  },
  GENESIS_TIME: 1545555600,
  DEFAULT_CONFIRMATIONS: 10,
};
