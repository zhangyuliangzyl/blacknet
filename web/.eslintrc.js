const path = require('path');

module.exports = {
  parserOptions: {
    ecmaFeatures: {
      legacyDecorators: true
    }
  },
  "parser": "babel-eslint",
  "extends": "eslint-config-airbnb",
  "rules": {
    "class-methods-use-this": 0,
    "max-len": 0,
    "camelcase": 0,
    "no-mixed-operators": 0,
    "react/jsx-filename-extension": 0,
    "react/jsx-props-no-spreading": 0,
    "jsx-a11y/control-has-associated-label": 0,
    "react/prop-types": 0,
    "react/destructuring-assignment": 0,
    "no-confusing-arrow": 0,
    "jsx-a11y/anchor-is-valid": 0,
    "react/jsx-no-target-blank": 0,
    "react/prefer-stateless-function": 0,
    "no-plusplus": 0,
    "no-await-in-loop": 0,
    "no-param-reassign": 0,
    "no-restricted-syntax": 0
  },
  "env": {
    "browser": true
  },
  "settings": {
    'import/resolver': {
      alias: {
        map: [
          ['@', path.resolve(__dirname, './src')],
        ],
        extensions: ['.ts', '.js', '.jsx', '.json']
      }
    }
  }
}
