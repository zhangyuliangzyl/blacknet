/*
 * Copyright (c) 2018-2019 Pavel Vasin
 * Copyright (c) 2019 Blacknet Team
 *
 * Licensed under the Jelurida Public License version 1.1
 * for the Blacknet Public Blockchain Platform (the "License");
 * you may not use this file except in compliance with the License.
 * See the LICENSE.txt file at the top-level directory of this distribution.
 */

package ninja.blacknet.core

import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.sync.withLock
import mu.KotlinLogging
import ninja.blacknet.Config
import ninja.blacknet.Config.mnemonics
import ninja.blacknet.Runtime
import ninja.blacknet.api.StakingInfo
import ninja.blacknet.crypto.*
import ninja.blacknet.db.BlockDB
import ninja.blacknet.db.LedgerDB
import ninja.blacknet.network.Node
import ninja.blacknet.time.SystemClock
import ninja.blacknet.time.delay
import ninja.blacknet.time.milliseconds.seconds
import ninja.blacknet.util.SynchronizedArrayList
import ninja.blacknet.util.sumByLong

private val logger = KotlinLogging.logger {}

/**
 * 持有工
 */
object Staker {
    private class StakerState(
            val publicKey: PublicKey,
            val privateKey: PrivateKey,
            var lastBlock: Hash = Hash.ZERO,
            var stake: Long = 0
    ) {
        fun updateImpl(state: LedgerDB.State) {
            lastBlock = state.blockHash
            stake = LedgerDB.get(publicKey)?.stakingBalance(state.height) ?: 0
        }
    }

    private val stakers = SynchronizedArrayList<StakerState>()

    init {
        if (Config.contains(mnemonics)) {
            runBlocking {
                Config[mnemonics].forEachIndexed { index, mnemonic ->
                    val privateKey = Mnemonic.fromString(mnemonic)
                    if (privateKey != null) {
                        startStaking(privateKey)
                    } else {
                        logger.warn("Invalid mnemonic $index")
                    }
                }
                val n = stakers.list.size
                if (n == 1)
                    logger.info("Started staking")
                else if (n > 1)
                    logger.info("Started staking with $n accounts")
            }
        }
    }

    @Volatile
    var awaitsNextTimeSlot: Job? = null
    private var coroutine: Job? = null
    private suspend fun implementation() {
        val job = Runtime.launch {
            val currTime = SystemClock.seconds
            val nextTimeSlot = currTime - currTime % PoS.TIME_SLOT + PoS.TIME_SLOT
            delay(nextTimeSlot.seconds - SystemClock.milliseconds)
        }
        awaitsNextTimeSlot = job
        job.join()
        awaitsNextTimeSlot = null

        if (!Config.regTest) {
            if (Node.isOffline())
                return

            if (Node.isInitialSynchronization())
                return
        }

        var state = LedgerDB.state()
        val currTime = SystemClock.seconds
        val timeSlot = currTime - currTime % PoS.TIME_SLOT
        if (timeSlot <= state.blockTime)
            return

        @Suppress("LABEL_NAME_CLASH")
        val block = stakers.mutex.withLock {
            for (i in stakers.list.indices) {
                val staker = stakers.list[i]

                if (staker.lastBlock != state.blockHash) {
                    BlockDB.mutex.withLock {
                        state = LedgerDB.state()
                        staker.updateImpl(state)
                    }
                }

                val pos = PoS.check(timeSlot, staker.publicKey, state.nxtrng, state.difficulty, state.blockTime, staker.stake)
                if (pos == Accepted) {
                    val block = Block.create(state.blockHash, timeSlot, staker.publicKey)
                    TxPool.fill(block)
                    return@withLock block.sign(staker.privateKey)
                }
            }
            return@withLock null
        }

        if (block != null) {
            logger.info("Staked ${block.first}")
            Node.broadcastBlock(block.first, block.second)
        }
    }

    suspend fun startStaking(privateKey: PrivateKey): Boolean = stakers.mutex.withLock {
        val publicKey = privateKey.toPublicKey()

        if (stakers.list.find { it.publicKey == publicKey } != null) {
            logger.info("${Address.encode(publicKey)} is already staking")
            return false
        }

        val staker = StakerState(publicKey, privateKey)
        BlockDB.mutex.withLock {
            val state = LedgerDB.state()
            staker.updateImpl(state)
        }
        if (staker.stake == 0L) {
            logger.warn("${Address.encode(publicKey)} has zero staking balance")
        }

        stakers.list.add(staker)
        if (stakers.list.size == 1) {
            coroutine = Runtime.rotate(::implementation)
        }
        return true
    }

    suspend fun stopStaking(privateKey: PrivateKey): Boolean = stakers.mutex.withLock {
        val publicKey = privateKey.toPublicKey()
        val i = stakers.list.indexOfFirst { it.publicKey == publicKey }
        if (i != -1) {
            stakers.list.removeAt(i)
        } else {
            logger.info("${Address.encode(publicKey)} is not staking")
            return false
        }
        if (stakers.list.size == 0) {
            coroutine!!.cancel()
            coroutine = null
            awaitsNextTimeSlot = null
        }
        return true
    }

    suspend fun isStaking(privateKey: PrivateKey): Boolean = stakers.mutex.withLock {
        return stakers.list.find { it.privateKey == privateKey } != null
    }

    suspend fun info(publicKey: PublicKey?): StakingInfo {
        val (nAccounts, weight) = stakers.mutex.withLock {
            if (publicKey == null) {
                Pair(stakers.list.size, stakers.list.sumByLong { it.stake })
            } else {
                val staker = stakers.list.find { it.publicKey == publicKey }
                if (staker != null)
                    Pair(1, staker.stake)
                else
                    Pair(0, 0L)
            }
        }
        val state = LedgerDB.state()
        val networkWeight = (PoS.MAX_DIFFICULTY / state.difficulty).toLong() / PoS.TARGET_BLOCK_TIME * PoS.TIME_SLOT
        val expectedTime = if (weight != 0L) PoS.TARGET_BLOCK_TIME * networkWeight / weight else 0L
        return StakingInfo(nAccounts, weight.toString(), networkWeight.toString(), expectedTime)
    }
}
