/*
 * Copyright (c) 2020 Blacknet Team
 *
 * Licensed under the Jelurida Public License version 1.1
 * for the Blacknet Public Blockchain Platform (the "License");
 * you may not use this file except in compliance with the License.
 * See the LICENSE.txt file at the top-level directory of this distribution.
 */

package ninja.blacknet

/**
 * Use of an application programming interface with this annotation does not fall under the heading of derived work.
 */
@SystemService
annotation class SystemService
